<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Client;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::with('client')->get();
        return view('comments.index', compact('comments'));
    }

    public function create()
    {
        $clients = Client::all();
        return view('comments.create', compact('clients'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'comment' => 'required|string|max:255',
        ]);

        Comment::create([
            'client_id' => $request->client_id,
            'comment' => $request->comment,
        ]);

        return redirect()->route('comments.index')->with('success', 'Comentario añadido con éxito');
    }
}


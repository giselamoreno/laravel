<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Client;
use App\Models\Product;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('client', 'products')->get();
        return view('orders.index', compact('orders'));
    }

    public function create()
    {
        $clients = Client::all();
        $products = Product::all();
        return view('orders.create', compact('clients', 'products'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'order_date' => 'required|date',
            'products.*.id' => 'required|exists:products,id',
            'products.*.quantity' => 'required|integer|min:1',
        ]);

        //validar stock productos
        foreach ($request->products as $product) {
            $productModel = Product::find($product['id']);
            if ($product['quantity'] > $productModel->quantity) {
                return redirect()->back()->withErrors(['quantity' => 'No hay suficiente stock disponible para el producto ']);
            }
        }

        //crear orden
        $order = Order::create([
            'client_id' => $request->client_id,
            'order_date' => $request->order_date,
        ]);

        //calcular el total con iva y sin iva
        $totalWithIva = 0;
        $totalWithoutIva = 0;

        foreach ($request->products as $product) {
            $productModel = Product::find($product['id']);
            $quantity = $product['quantity'];
            $price = $productModel->price;
            $category = $productModel->category;
            $ivaPercentage = $category->iva_percentage;

            //calcular el precio total del producto sin IVA
            $totalProductWithoutIva = $price * $quantity;
            $totalWithoutIva += $totalProductWithoutIva;

            //calcular el precio total del producto con IVA
            $totalProductWithIva = $price * $quantity * (1 + ($ivaPercentage / 100));
            $totalWithIva += $totalProductWithIva;

            // poner el producto en la comanda con el precio y la cantidad
            $order->products()->attach($product['id'], [
                'quantity' => $quantity,
                'price' => $price,
            ]);

            //restar la cantidad al stock
            $productModel->quantity -= $quantity;
            $productModel->save();
        }

        //guardar el total con iva y sin iva en la comanda
        $order->total_price = $totalWithoutIva;
        $order->total_price_with_iva = $totalWithIva;
        $order->save();

        return redirect()->route('orders.create');
    }
}

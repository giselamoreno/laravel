<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::all();
        return view('clients.index', ['clients' => $clients]);
    }

    public function delete(Client $client): RedirectResponse
    {
        $client->delete();
        return Redirect::route('clients.index');
    }

    public function insert(Request $request)
    {


        $client = new Client();
        $client->name = $request->name;
        $client->phone = $request->phone;
        $client->address = $request->address;
        $client->save();

        return redirect()->route('clients.index');
    }
}

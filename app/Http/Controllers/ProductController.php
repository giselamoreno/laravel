<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use App\Models\Category;

class ProductController extends Controller
{
    public function index(): View {

        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function delete(Product $product): RedirectResponse
    {

        $product->delete();

        return Redirect::route('products.index');
    }

    public function insert(Request $request)
    {

        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->category_id;
        $product->save();


        return redirect()->route('products.index');
    }

    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }



}

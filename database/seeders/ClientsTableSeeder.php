<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            ['name' => 'Gisela', 'phone' => '123456789', 'address' => 'sabadell'],
            ['name' => 'Ivan', 'phone' => '987654321', 'address' => 'terrassa'],
            ['name' => 'nelu', 'phone' => '555555555', 'address' => 'candeu'],
            ['name' => 'Fran', 'phone' => '444444', 'address' => 'candeufalso'],
            ['name' => 'Nora', 'phone' => '1232131', 'address' => 'candeu2'],
            ['name' => 'almatruj', 'phone' => '21312313', 'address' => 'jaca'],
        ]);
    }
}

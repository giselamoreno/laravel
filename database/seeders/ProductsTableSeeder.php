<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('products')->insert([
            ['name' => 'espada', 'description' => 'ten cuidao que corta', 'price' => 23.99, 'quantity' => 7, 'category_id' => 1],
            ['name' => 'casco hierro', 'description' => 'te cubre la cabeza', 'price' => 15.99, 'quantity' => 5, 'category_id' => 2],
            ['name' => 'pocion vida', 'description' => 'te da vida', 'price' => 13.99, 'quantity' => 2, 'category_id' => 3],

        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('categories')->insert([
            ['name' => 'Armas', 'iva_percentage' => 10.00],
            ['name' => 'Armaduras', 'iva_percentage' => 15.00],
            ['name' => 'Pociones', 'iva_percentage' => 5.00]
        ]);

    }
}


<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Lista de Comentarios') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                        <tr>
                            <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Id
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Cliente
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Comentario
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Fecha
                            </th>
                        </tr>
                        </thead>

                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($comments as $comment)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $comment->client->id }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $comment->client->name }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $comment->comment }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $comment->created_at->format('d/m/Y H:i:s') }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4 flex justify-end">
                        <a href="{{ route('comments.create') }}">Añadir Comentario</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
